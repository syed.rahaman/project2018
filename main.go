package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"

	//"github.com/Hepri/gin-jsonschema"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

var (
	repeat int
	db     *sql.DB
)

// var todoSchema string = `
// {
// 	"title": "todoItem",
//       "type": "object",
//       "required": [
//         "name",
//         "status",
//         "task"
//       ],
//       "properties": {
//         "name": {
//           "type": "string"
//         },
//         "task": {
//           "type": "string"
//         },
//         "status": {
//           "type": "string"
//         }
//       },
//       //"example": "{\"name\":\"task1\", \"task\":\"buy milk\", \"status\":\"pending\"}"
// }
// `

type TODODATA struct {
	NAME   string `json:"name" binding:"required"`
	TASK   string `json:"task" binding:"required"`
	STATUS string `json:"status" binding:"required"`
}
type Todolist struct {
	Id     int
	Name   string
	Task   string
	Status string
}

// func handlerFunc(c *gin.Context) {
// 	c.Status(http.StatusOK)
// }

func todoCreateFunc(c *gin.Context) {

	var tododata TODODATA
	c.BindJSON(&tododata)
	// if _, err := db.Exec("DROP TABLE todos"); err != nil {
	// 	c.String(http.StatusInternalServerError,
	// 		fmt.Sprintf("Error creating database table: %q", err))
	// 	return
	// }
	if _, err := db.Exec("CREATE TABLE IF NOT EXISTS todos (id serial PRIMARY KEY, name text, task text, status text)"); err != nil {
		c.String(http.StatusInternalServerError,
			fmt.Sprintf("Error creating database table: %q", err))
		return
	}

	if _, err := db.Exec("INSERT INTO todos (name, task, status) VALUES ('" + tododata.NAME + "','" + tododata.TASK + "','" + tododata.STATUS + "')"); err != nil {
		c.String(http.StatusInternalServerError,
			fmt.Sprintf("Error incrementing task: %q", err))
		return
	}
	c.JSON(200, gin.H{
			"result": "success",
	})
}

func todoReadFunc(c *gin.Context) {
	rows, err := db.Query("SELECT * FROM todos")
	if err != nil {
		c.String(http.StatusInternalServerError,
			fmt.Sprintf("Error reading task: %q", err))
		return
	}
	var (
		todolist  Todolist
		todolists []Todolist
	)
	for rows.Next() {
		if err := rows.Scan(&todolist.Id, &todolist.Name, &todolist.Task, &todolist.Status); err != nil {
			c.String(http.StatusInternalServerError,
				fmt.Sprintf("Error scanning task: %q", err))
			return
		}
		// c.JSON(200, gin.H{
		// 	"id":     todolist.id,
		// 	"name":   todolist.name,
		// 	"task":   todolist.task,
		// 	"status": todolist.status,
		// })
		todolists = append(todolists, todolist)
	}
	defer rows.Close()
	c.JSON(200, gin.H{
		"result": todolists,
	})
}

func todoDelFunc(c *gin.Context) {
	id := c.Param("id")
	message := " The deleted task id is" + id
	c.String(http.StatusOK, message)
}

func todoUpdateFunc(c *gin.Context) {
	id := c.Query("id")
	task := c.PostForm("task")
	status := c.PostForm("status")
	message := " The updated task id is " + id + " and task is " + task + " and status is " + status
	c.String(http.StatusOK, message)
}

// func repeatFunc(c *gin.Context) {
// 	var buffer bytes.Buffer
// 	for i := 0; i < repeat; i++ {
// 		buffer.WriteString("Hello from Go!")
// 	}
// 	c.String(http.StatusOK, buffer.String())
// }
func main() {
	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal("$PORT must be set")
	}

	var err error
	// tStr := os.Getenv("REPEAT")
	// repeat, err = strconv.Atoi(tStr)
	// if err != nil {
	// 	log.Print("Error converting $REPEAT to an int: %q - Using default", err)
	// 	repeat = 5
	// }

	db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatalf("Error opening database: %q", err)
	}
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	//r.GET("/repeat", repeatFunc)
	//r.POST("/validate", schema.Validate(handlerFunc, todoSchema))
	//r.POST("/todoitem", schema.Validate(todoCreateFunc, todoSchema))
	r.POST("/todoitem", todoCreateFunc)
	r.GET("/todoitems", todoReadFunc)
	r.DELETE("/todoitem/{id}", todoDelFunc)
	r.PUT("/todoitem", todoUpdateFunc)
	r.Run() // listen and serve on 0.0.0.0:8080
}
